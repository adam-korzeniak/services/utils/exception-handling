package com.adamkorzeniak.utils.exceptions;

import com.adamkorzeniak.utils.exceptions.model.ErrorInfo;
import com.adamkorzeniak.utils.exceptions.model.ErrorResponse;
import com.adamkorzeniak.utils.exceptions.model.exception.BusinessException;
import com.adamkorzeniak.utils.exceptions.model.exception.ErrorType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
        if (!CollectionUtils.isEmpty(supportedMethods)) {
            headers.setAllow(supportedMethods);
        }
        return buildExceptionResponse(
                ErrorType.METHOD_NOT_ALLOWED, HttpStatus.METHOD_NOT_ALLOWED, headers, ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleBindException(
            BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        var errors = ex.getAllErrors().stream()
                .map(field -> buildError(ErrorType.BAD_REQUEST, field.toString()))
                .collect(Collectors.toList());
        ErrorResponse errorResponse = buildErrorResponse(errors);
        return new ResponseEntity<>(errorResponse, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BusinessException.class)
    protected ResponseEntity<ErrorResponse> businessException(BusinessException exc) {
        var error = buildError(exc.getCode(), exc.getTitle(), exc.getMessage());
        ErrorResponse errorResponse = buildErrorResponse(error);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    protected ResponseEntity<ErrorResponse> accessDeniedException(AccessDeniedException exc) {
        var error = new ErrorResponse.Error("ACCESS DENIED", "Access Denied", exc.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(Collections.singletonList(error));
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = Throwable.class)
    protected ResponseEntity<ErrorResponse> defaultException(Throwable exc) {
        String exceptionMessage = Optional.ofNullable(exc.getCause())
                .map(Throwable::getMessage)
                .orElse(exc.getMessage());
        var error = buildError(ErrorType.INTERNAL_SERVER_ERROR, exceptionMessage);
        ErrorResponse errorResponse = buildErrorResponse(error);
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> buildExceptionResponse(ErrorType errorType, HttpStatus httpStatus, HttpHeaders headers, String exceptionMessage) {
        var error = buildError(errorType, exceptionMessage);
        ErrorResponse errorResponse = buildErrorResponse(error);
        return new ResponseEntity<>(errorResponse, headers, httpStatus);
    }

    private ErrorResponse.Error buildError(String code, String title, String message) {
        return new ErrorResponse.Error(code, title, message);
    }

    private ErrorResponse.Error buildError(ErrorInfo error, String message) {
        return buildError(error.getCode(), error.getTitle(), message);
    }

    private ErrorResponse buildErrorResponse(ErrorResponse.Error error) {
        return buildErrorResponse(Collections.singletonList(error));
    }

    private ErrorResponse buildErrorResponse(List<ErrorResponse.Error> errors) {
        return new ErrorResponse(errors);
    }

}

