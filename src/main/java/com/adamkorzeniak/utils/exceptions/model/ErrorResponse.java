package com.adamkorzeniak.utils.exceptions.model;

import lombok.Generated;
import lombok.Value;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Value
@Generated
public class ErrorResponse {

    UUID errorId = UUID.randomUUID();
    Long timestamp = Instant.now().toEpochMilli();
    List<Error> errors;

    @Value
    @Generated
    public static class Error {
        String code;
        String title;
        String message;
    }
}
